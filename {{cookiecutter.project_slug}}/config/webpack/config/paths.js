const path = require('path')
const fs = require('fs')
const url = require('url')

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd())
const resolveApp = relativePath => path.resolve(appDirectory, relativePath)

const envPublicUrl = process.env.PUBLIC_URL

function ensureSlash(needlePath, needsSlash) {
    const hasSlash = needlePath.endsWith('/')
    if (hasSlash && !needsSlash) {
        return needlePath.substr(needlePath, needlePath.length - 1)
    } else if (!hasSlash && needsSlash) {
        return `${needlePath}/`
    }
    return needlePath
}

const getPublicUrl = appPackageJson =>
    envPublicUrl || require(appPackageJson).homepage

// We use `PUBLIC_URL` environment variable or "homepage" field to infer
// "public path" at which the app is served.
// Webpack needs to know it to put the right <script> hrefs into HTML even in
// single-page apps that may serve index.html for nested URLs like /todos/42.
// We can't use a relative path in HTML because we don't want to load something
// like /todos/42/static/js/bundle.7289d.js. We have to know the root.
function getServedPath(appPackageJson) {
    const publicUrl = getPublicUrl(appPackageJson)
    const servedUrl =
        envPublicUrl || (publicUrl ? url.parse(publicUrl).pathname : '/')
    return ensureSlash(servedUrl, true)
}

module.exports = {
    appBuild: resolveApp('build'),
    appNodeModules: resolveApp('node_modules'),
    appHtml: resolveApp('{{cookiecutter.project_slug}}/client/templates/base.html'),
    appIndexJs: resolveApp('{{cookiecutter.project_slug}}/client/static/apps/Home.jsx'),
    entry: {
        "main_style": "{{cookiecutter.project_slug}}/client/static/sass/project.sass/",
        "main_js": "{{cookiecutter.project_slug}}/client/static/js/project.js"
    },
    appPackageJson: resolveApp('package.json'),
    appPublic: resolveApp('{{cookiecutter.project_slug}}/client/static'),
    appSrc: resolveApp('{{cookiecutter.project_slug}}/client/static'),
    publicUrl: getPublicUrl(resolveApp('package.json')),
    servedPath: getServedPath(resolveApp('package.json')),
    yarnLockFile: resolveApp('yarn.lock'),
}
